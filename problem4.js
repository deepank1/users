

function problem4(users) {
    
    if(Array.isArray(users)){
        let emailArray = [];

        for(let index=0; index<users.length; index++){
           emailArray.push(users[index].email);
        }

        return emailArray;
    }
    return [];
}

module.exports = problem4;