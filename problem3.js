
function problem3(users) {
    
    if(Array.isArray(users)){
        let usersCopy = [...users];
        usersCopy.sort((a,b)=>{ 
            if(a.last_name.toLowerCase()<b.last_name.toLowerCase()){
                return -1;
            }else if(a.last_name.toLowerCase()>b.last_name.toLowerCase()){
                return 1;
            }
            else{
                return 0;
            }
        });
        return usersCopy;

    }
    return [];

}

module.exports = problem3;