

let users = require("../users.js");

let problem2 = require("../problem2.js");

let result = problem2(users);

if(!Array.isArray(result)){
    console.log("Last user is " + result.first_name + " " + result.last_name +
    " and can be contacted on " + result.email);
}
else{
    console.log("Invalid Input")
}