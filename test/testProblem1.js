let id = 100;

let users = require("../users.js");

let problem1 = require("../problem1.js");

let result = problem1(users, id);

if(!Array.isArray(result)){
    console.log(result.first_name +" " + result.last_name + " is a " +
    result.gender + " and can be contacted on " + result.email);
}
else{
   console.log("Invalid Input")
}