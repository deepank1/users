

function problem5(users) {
    
    let allMaleArray = [];

    if(Array.isArray(users)){
        for(let index=0; index<users.length; index++){
            if(users[index].gender.toLowerCase()==="male"){
                allMaleArray.push([(users[index].first_name+" "+users[index].last_name),users[index].email]);
            }
        }
        return allMaleArray;
    }
    return [];
}

module.exports = problem5;